**It is an online skill registry platform for wipro employees. A platform to showcase their skills.**


**Tools and Technologies used :**

    - React JS
    - Redux JS
    - ES6
    - JavaScript
    - CSS
    - HTML
    
	

**Local Setup -**

  1. Clone the repository.
  2. Run command "npm install" inside project root directory to install dependencies.
  3. Run command "npm start" to run the application.

**NOTE** - Change the API call base url in ```src/constants/backendContants.js``` to ```http://localhost:<port>/```

For any help, drop a mail to **varshant.14@gmail.com**